stl-manual (3.30-16) unstable; urgency=medium

  * QA upload.
  * debian/control: updated VCS field.
  * debian/salsa-ci.yml: created to perform Salsa CI tests.

 -- Gleisson Jesuino Joaquim Cardoso <gleissoncg2@gmail.com>  Wed, 10 Jun 2020 04:00:15 -0300

stl-manual (3.30-15) unstable; urgency=medium

  * QA upload.
  * Run wrap-and-sort.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Updated short and long descriptions. (Closes: #702622)
      - Using a secure URI in Homepage field.
  * debian/copyright:
      - Migrated to 1.0 format.
      - Updated all data.
      - Updated upstream and packaging license to MIT.
  * debian/patches/debian-changes-3.30-12: updated header and data.
  * debian/upstream/metadata: created.
  * debian/rules:
      - Added dh sequencer.
      - Migrated to new (reduced) format.
      - Removed trash.
  * debian/tests: created to perform CI tests.
  * debian/watch: added a fake site to current status of the original upstream
      homepage.

 -- Gleisson Jesuino Joaquim Cardoso <gleissoncg2@gmail.com>  Wed, 20 May 2020 02:36:57 -0300

stl-manual (3.30-14) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as mantainer. (see #846841)
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Bumped Standards-Version to 4.5.0.

 -- Gleisson Jesuino Joaquim Cardoso <gleissoncg2@gmail.com>  Thu, 14 May 2020 11:36:07 -0300

stl-manual (3.30-13.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Switch the binary package compressor from bzip2 to xz (Closes: #833214).

 -- Andrey Rahmatullin <wrar@debian.org>  Sat, 03 Dec 2016 02:35:37 +0500

stl-manual (3.30-13) unstable; urgency=low

  * Bump Standards-Version to 3.9.3
  * New maintainer (Closes: #654555)

 -- cento <ignorante@autistici.org>  Sun, 08 Apr 2012 00:07:02 +0200

stl-manual (3.30-12) unstable; urgency=low

  * Bump Standards-Version to 3.9.1
  * Bump debhelper compatibility to 8

 -- Francois Marier <francois@debian.org>  Wed, 22 Dec 2010 17:26:33 +1300

stl-manual (3.30-11) unstable; urgency=low

  * Bump Standards-Version to 3.8.4
  * Switch to 3.0 (quilt) source format
  * Add dependency on ${misc:Depends}

 -- Francois Marier <francois@debian.org>  Wed, 10 Feb 2010 23:44:36 +1300

stl-manual (3.30-10) unstable; urgency=low

  * Bump Standards-Version to 3.8.2
  * Bump debhelper compatibility to 7
  * Switch repo to git and update VCS fields

 -- Francois Marier <francois@debian.org>  Sun, 05 Jul 2009 12:33:44 +1200

stl-manual (3.30-9) unstable; urgency=low

  * Set doc-base section to Programming
  * Bump debhelper compatibility to 6

 -- Francois Marier <francois@debian.org>  Tue, 03 Jun 2008 14:48:20 +1200

stl-manual (3.30-8) unstable; urgency=low

  * debian/rules: move everything into the indep target
  * Bump Standards-Version up to 3.7.3 (no other changes)

 -- Francois Marier <francois@debian.org>  Mon, 24 Dec 2007 02:48:42 -0500

stl-manual (3.30-7) unstable; urgency=low

  * Compress the deb with bzip2 instead of gzip
  * Bump debhelper compatibility and dependency to 5
  * Move the URL from the description into the Homepage field
  * Mention the collab-maint repo for it

 -- Francois Marier <francois@debian.org>  Thu, 22 Nov 2007 12:06:11 +1300

stl-manual (3.30-6) unstable; urgency=low

  * Make the function names bold to be more easily readable
  * Moved debhelper to Build-Depends
  * Bump Standards-Version up to 3.7.2

 -- Francois Marier <francois@debian.org>  Sun, 25 Jun 2006 21:30:44 -0400

stl-manual (3.30-5) unstable; urgency=low

  * New maintainer (closes: #314845)
  * Update Standards-Version to 3.6.1
  * Add URL to the package description and remove README.Debian since it's no
    longer very useful
  * Update copyright file
  * Brand new rules file and set debhelper compatiblity to version 4
  * Shorten the doc-base asbtract

 -- Francois Marier <francois@debian.org>  Sun, 19 Jun 2005 16:25:12 -0400

stl-manual (3.30-4) unstable; urgency=low

  * Add download information to copyright. (Closes: #121858)
  * Updated Standards-Version; changed Build-Depends to
    Build-Depends-Indep.

 -- Ivo Timmermans <ivo@debian.org>  Tue,  9 Apr 2002 11:11:04 +0200

stl-manual (3.30-3) unstable; urgency=low

  * Compress changelog. (Closes: #103081)

 -- Ivo Timmermans <ivo@debian.org>  Mon, 30 Jul 2001 22:41:04 +0200

stl-manual (3.30-2) unstable; urgency=low

  * Remove postinst and prerm scripts. (Closes: #94357)
  * Updated various copyright fields to the more up to date versions.
  * Replaced &lt/&gt/&amp with &lt;/&gt;/&amp; in string_discussion.html.
    (Closes: #42978)

 -- Ivo Timmermans <ivo@debian.org>  Wed, 18 Apr 2001 13:31:28 +0200

stl-manual (3.30-1) unstable; urgency=low

  * New upstream release. (Closes: #49582, #64376)
  * New maintainer. (Closes: #93896)
  * Updated standards-version to 3.5.2.
  * Added build dependencies.
  * Moved everything to /usr/share/doc. (Closes: #72739, #74123, #91667)
  * Recommend www-browser instead of web-browser. (Closes: #63004)

 -- Ivo Timmermans <ivo@debian.org>  Mon, 16 Apr 2001 23:38:50 +0200

stl-manual (3.11-3) frozen unstable; urgency=low

  * upload w/ orig.tar.gz

 -- Heiko Schlittermann <heiko@lotte.sax.de>  Tue, 22 Dec 1998 10:01:57 +0100

stl-manual (3.11-2) frozen unstable; urgency=low

  * register is to doc-base (#30625)
  * try to get it into slink (#30628)
    - no binaries are included here, so I think, this package
    can't do too much harm to be excluced from slink ;-)
    - we don't have a comprehensive STL documentation so far

 -- Heiko Schlittermann <heiko@lotte.sax.de>  Sat, 12 Dec 1998 12:17:45 +0100

stl-manual (3.11-1) unstable; urgency=low

  * initial release

 -- Heiko Schlittermann <heiko@lotte.sax.de>  Wed,  9 Dec 1998 11:07:13 +0100
